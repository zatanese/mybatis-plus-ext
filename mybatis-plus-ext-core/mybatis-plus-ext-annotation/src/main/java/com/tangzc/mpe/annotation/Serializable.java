package com.tangzc.mpe.annotation;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import org.apache.ibatis.type.TypeHandler;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 表示字段可被序列化
 *
 * @author don
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@TableField
public @interface Serializable {

    /**
     * 需要配合 @TableName(autoResultMap = true)
     *
     * @return
     */
    @AliasFor(annotation = TableField.class, attribute = "typeHandler")
    Class<? extends TypeHandler> typeHandler() default JacksonTypeHandler.class;
}
